jQuery(document).ready(function() {


	//------------------------------------------------------------------------
	//						LIGHT GALLERY
	//------------------------------------------------------------------------ 
	
    $('.lightgallery').lightGallery();

	
	//------------------------------------------------------------------------
	//						SMART MENU
	//------------------------------------------------------------------------ 	
	
	// always emulate touch mode
	$.SmartMenus.prototype.isTouchMode = function() {
		return true;
	};
	
	
	//------------------------------------------------------------------------
	//						ICHECK
	//------------------------------------------------------------------------ 	

	$('.skin-flat input').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});
	
	
	//------------------------------------------------------------------------
	//						CUSTOM SCTRIPS
	//------------------------------------------------------------------------

	$(".clickToShow").each(function() {
		var $this = $(this);
		var shortNumber = $this.text().substring(0, 5);
		var $btn = $('<div class="clickToShowButton">' + shortNumber + '(' + $this.attr('data-target') + ')</div>');
		$this.hide().after($btn);
		$btn.click(function() {
			$.ajax({
				'url': '/site/update-clicked',
				'method': 'POST',
				'data': {post: $this.data('post'), object: $this.data('object')}
			});
			$this.show();
			$btn.hide();
		});
	});

	$("[type=button]").button();
	$("[type=button]").click(function(){
	  $(this).button('toggle');
	  if ($(this).text()==="Closed"){
		$(this).button('open');
	  }
	  else {
		$(this).button('reset');
	  }
	});

	$('.dropdown-menu-links').click(function () {
		setCookie('city', encodeURIComponent($(this).text()), 30)
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

				
   //------------------------------------------------------------------------
   //						FIXED NAVIGATION SETTINGS
   //------------------------------------------------------------------------ 	
	
	$(window).scroll(function(){
		 if ( $(this).scrollTop() > $('header').outerHeight() ) {
			  $('.navbar-fixed').addClass('showup')
		 } else {
			  $('.navbar-fixed').removeClass('showup')
		 }
	});
	

	//------------------------------------------------------------------------
	//						GO TO TOP SETTINGS
	//------------------------------------------------------------------------
	$(window).scroll(function () {
		if ($(this).scrollTop() > 400) {
		  $('.go-to-top').fadeIn();
		} else {$('.go-to-top').fadeOut();}
		$('.go-to-top').smoothScroll({
		  speed: 900,
		  scrollTarget: 'body',
		  easing: 'swing'
		});
	});
	
	
	//------------------------------------------------------------------------
	//	Full height header
	//------------------------------------------------------------------------

	$('#main-menu').bind('click.smapi', function(e, item) {});	
	
		
	//------------------------------------------------------------------------
	//	OWR CAROUSEL 2 http://www.owlcarousel.owlgraphic.com
	//------------------------------------------------------------------------ 
	
	var vipBanner = $('.vip-banner');

	vipBanner.owlCarousel({		
		autoplay: true,
		autoplayTimeout: 6000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: true,
		margin: 0,		
		dots: false, 
		mouseDrag: false,	
		nav: true, 
		items: 1,	
		navText: false			
	});
	
	
	var premiumBanner = $('.premium-banner');

	premiumBanner.owlCarousel({
		autoplay: true,
		autoplayTimeout: 8000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: true,
		margin: 15,		
		dots: false,     
		nav: true,    
		navText: false,	
		responsive : {
			 // breakpoint from 0 up
			 0 : {
						
			 },
			 // breakpoint from 320 up
			 320 : {
					items : 1,			
			 },			 
			 // breakpoint from 480 up
			 480 : {
					items : 2,
			 },
			 // breakpoint from 768 up
			 768 : {
					items : 2,
			 },
			 // breakpoint from 992 up
			 992 : {
					items : 2,
			 },	
			 // breakpoint from 992 up
			 992 : {
					items : 2,
			 }					 
		}			
	});
	
	
	var carouselList = $('.carousel-list');

	carouselList.owlCarousel({
		autoplay: false,
		autoplayTimeout: 3000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: true,
		margin: 10,		
		dots: false,     
		nav: true,    
		navText: false,
        onInitialize: function (event) {
            if (this._items.length <= 1) {
                this.settings.loop = false;
            }
        },
		responsive : {
			 // breakpoint from 0 up
			 0 : {
						
			 },
			 // breakpoint from 320 up
			 320 : {
					items : 1,			
			 },			 
			 // breakpoint from 480 up
			 480 : {
					items : 2,
			 },
			 // breakpoint from 768 up
			 768 : {
					items : 3,
			 },
			 // breakpoint from 992 up
			 992 : {
					items : 4,
			 },	
			 // breakpoint from 992 up
			 992 : {
					items : 4,
			 }					 
		}			
	});
	
	
	var vacancyList = $('.vacancy-list');

	vacancyList.owlCarousel({
		autoplay: false,
		autoplayTimeout: 3000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: true,
		margin: 10,		
		dots: false,     
		nav: true,    
		navText: false,	
		responsive : {
			 // breakpoint from 0 up
			 0 : {
						
			 },
			 // breakpoint from 320 up
			 320 : {
					items : 1,			
			 },			 
			 // breakpoint from 480 up
			 480 : {
					items : 2,
			 },
			 // breakpoint from 768 up
			 768 : {
					items : 3,
			 },
			 // breakpoint from 992 up
			 992 : {
					items : 4,
			 },	
			 // breakpoint from 992 up
			 992 : {
					items : 4,
			 }					 
		}			
	});
	
	
	// var newsList = $('.news-list');
    //
	// newsList.owlCarousel({
	// 	autoplay: false,
	// 	autoplayTimeout: 3000,
	// 	smartSpeed: 1100,
	// 	autoplayHoverPause:true,
	// 	loop: true,
	// 	margin: 10,
	// 	dots: false,
	// 	nav: true,
	// 	navText: false,
	// 	responsive : {
	// 		 // breakpoint from 0 up
	// 		 0 : {
	//
	// 		 },
	// 		 // breakpoint from 320 up
	// 		 320 : {
	// 				items : 1,
	// 		 },
	// 		 // breakpoint from 480 up
	// 		 480 : {
	// 				items : 2,
	// 				margin: 4
	// 		 },
	// 		 // breakpoint from 768 up
	// 		 768 : {
	// 				items : 3,
	// 		 },
	// 		 // breakpoint from 992 up
	// 		 992 : {
	// 				items : 4,
	// 		 },
	// 		 // breakpoint from 992 up
	// 		 992 : {
	// 				items : 4,
	// 		 }
	// 	}
	// });
	
		
	var placeImages = $('.place-gallery');

	placeImages.owlCarousel({
		autoplay: false,
		autoplayTimeout: 3000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: true,
		margin: 0,		
		dots: false,     
		nav: true,    
		navText: false,	
		responsive : {
			 // breakpoint from 0 up
			 0 : {
						
			 },
			 // breakpoint from 320 up
			 320 : {
					items : 1,			
			 },			 
			 // breakpoint from 480 up
			 480 : {
					items : 2,
			 },
			 // breakpoint from 768 up
			 768 : {
					items : 2,
			 },
			 // breakpoint from 992 up
			 992 : {
					items : 4,
			 },	
			 // breakpoint from 992 up
			 1170 : {
					items : 5,
			 }					 
		}			
	});
	
	
	//------------------------------------------------------------------------
	// MAGNIFIC POPUP http://dimsemenov.com/plugins/magnific-popup/
	//------------------------------------------------------------------------

	/* IFRAME POPUP */
	$('.magnificIframe').magnificPopup({
		removalDelay: 500, //delay removal by X to allow out-animation		
		type: 'iframe', 
		closeBtnInside: true,
		closeOnContentClick: true,	
		midClick: true, // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.		
		iframe: {
			markup: '<div class="mfp-iframe-scaler mfp-with-anim">'+
					 '<div class="mfp-close"></div>'+
					 '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
					 '<div class="mfp-title">Some caption</div>'+
				  '</div>'
	},
	callbacks: {
		markupParse: function(template, values, item) {
			values.title = item.el.attr('title');	
		},
		beforeOpen: function() {
			this.st.mainClass = this.st.el.attr('data-effect');
		}	 
	}	  
	});

	/* INLINE POPUP */
	$('.magnificPopup').magnificPopup({  
		removalDelay: 500, //delay removal by X to allow out-animation
		callbacks: {
			beforeOpen: function() {
				this.st.mainClass = this.st.el.attr('data-effect');
			}
	},
		midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.         
	});

	/* SINGLE IMAGE */
	$('.magnificImage').magnificPopup({  
		type: 'image',
		tLoading: '',
		removalDelay: 500, //delay removal by X to allow out-animation
		callbacks: {
			beforeOpen: function() {
				// just a hack that adds mfp-anim class to markup 
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		},
		closeBtnInside: false,
		closeOnContentClick: true,
		midClick: true         
	});

	/* GALLERY MODE */
	var groups = {};
	$('.magnificGallery').each(function(){
	var id = $(this).attr('data-group');
		if(!groups[id]) {
			groups[id] = [];
		}
		groups[id].push( this );
	});

	$.each(groups, function() {

	$(this).magnificPopup({
		type: 'image',
		tLoading: '',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		removalDelay: 500, //delay removal by X to allow out-animation	 
		callbacks: {
				beforeOpen: function() {
				// just a hack that adds mfp-anim class to markup 
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		},
		closeBtnInside: false,
		closeOnContentClick: true,
		midClick: true
		})

	});

	// Add it after jquery.magnific-popup.js and before first initialization code
	$.extend(true, $.magnificPopup.defaults, {
		tClose: 'Закрыть (Esc)', // Alt text on close button
		tLoading: 'Загрузка...', // Text that is displayed during loading. Can contain %curr% and %total% keys
		gallery: {
			tPrev: 'Предыдущий', // Alt text on left arrow
			tNext: 'Следующий)', // Alt text on right arrow
			tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
		},
			image: {
			tError: '<a href="%url%">Изоображение</a> не может быть загружено.' // Error message when image could not be loaded
		},
			ajax: {
			tError: '<a href="%url%">Контент</a> пустой.' // Error message when ajax request failed
		}
	});
		
	
	
   //------------------------------------------------------------------------
   //	SELECTSIZE https://selectize.github.io/selectize.js/
   //------------------------------------------------------------------------	
	
	$('.selectize').selectize({
		create: true,
		persist: false,
		sortField: {
			field: 'text',
			direction: 'asc'
		},
		dropdownParent: 'body'
	});

	
	//------------------------------------------------------------------------
	//	jQuery Bar Rating http://antenna.io/demo/jquery-bar-rating/examples/
	//------------------------------------------------------------------------ 

	$('.rate-stars-vote').barrating({
		theme: 'fontawesome-stars'
	});	
	
	
	//------------------------------------------------------------------------
	//						BOOTSTRAP DATEPICKER
	//------------------------------------------------------------------------
	
	$('.datepicker').datepicker({
		language: 'ru',
		format: "dd MM yyyy",
		ignoreReadonly: true,	
	});	

	//------------------------------------------------------------------------
	//						BOOTSTRAP POPOVER
	//------------------------------------------------------------------------
		
	$('[data-toggle="popover"]').popover({
		trigger: "focus",
		container: "body",   
	}); 
	

});

function ShowOrHide(id) {
	$('#'+id).toggle();
}
