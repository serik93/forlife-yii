<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'RequestPasswordResetToken';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- PAGE WRAPPER BEGIN -->
	<div class="page-wrapper">
		<div class="container container-sm">
			<div class="auth-page">
				<div class="logo">
					<img src="/img/logo2.png">
				</div>
				<div class="auth-box">
					<div class="row" data-gutter="15">
						<div class="col-sm-6">
							<div class="auth-welcome auth-box-body">
							<h1>Добро пожаловать</h1>
							<p>Интернет-портал Forlife.kz объединяет
							такие сайты, как Бизнес-справочник, Дисконтная система, Купонная система Казахстана и интернет-канал TVForlife.kz.
							Наша основная задача– создать
							по-настоящему выгодную и универсальную систему взаимодействия между всеми участниками потребительского рынка.</p>            				
         				
							</div>
						</div>
						<div class="col-sm-6">
							<div class="auth-form auth-box-body border-left">
								<?php $form = ActiveForm::begin([
								    'id' => 'requestresetpassword-form',
								    'layout' => 'horizontal',
								    'fieldConfig' => [
								        'template' => "{label}\n{input}<span class=\"error\">{error}</span>"//,
								        // 'labelOptions' => ['class' => 'col-lg-1 control-label'],
								    ],
								]); ?>

									
									<span class="information">Введите Ваш E-mail адрес. На этот адрес будет отправлена письмо с инструкцией для смены Вашего пароля.</span>

									<?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail адрес'])->label(false) ?>

									<div class="form-group">
										<?= Html::submitButton('Сбросить пароль', ['class' => 'btn btn-green', 'name' => 'resetpassword-button']) ?>
									</div>
								<?php ActiveForm::end(); ?>
								<div class="auth-social">
									<p>Войти с помощью соц. сетей</p>
									<ul class="list-inline">
										<li><a class="btn btn-facebook btn-soc" href="#"><i class="fa fa-facebook-f"></i></a></li>
										<li><a class="btn btn-instagram btn-soc" href="#"><i class="fa fa-instagram"></i></a></li>
										<li><a class="btn btn-vk btn-soc" href="#"><i class="fa fa-vk"></i></a></li>
										<li><a class="btn btn-gp btn-soc" href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
								<p class="text-center">Уже зарегистрированы? <a href="/auth/login" class="text-sb">Войдите</a></p>								
							</div>
						</div>			
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/. PAGE WRAPPER END -->