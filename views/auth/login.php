<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- PAGE WRAPPER BEGIN -->
    <div class="page-wrapper">
        <div class="container container-sm">
            <div class="auth-page">
                <div class="logo">
                    <img src="/img/logo2.png">
                </div>
                <div class="auth-box">
                    <div class="row" data-gutter="15">
                        <div class="col-sm-6">
                            <div class="auth-welcome auth-box-body">
                            <h1>Добро пожаловать</h1>
                            <p>Интернет-портал Forlife.kz объединяет
                            такие сайты, как Бизнес-справочник, Дисконтная система, Купонная система Казахстана и интернет-канал TVForlife.kz.
                            Наша основная задача– создать
                            по-настоящему выгодную и универсальную систему взаимодействия между всеми участниками потребительского рынка.</p>                           
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="auth-form auth-box-body border-left">

                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'layout' => 'horizontal',
                                'fieldConfig' => [
                                    'template' => "{label}\n{input}<span class=\"error\">{error}</span>"//,
                                    // 'labelOptions' => ['class' => 'col-lg-1 control-label'],
                                ],
                            ]); ?>

                                <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail адрес'])->label(false) ?>

                                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>

                                <div class="form-group">
                                    <!-- <div class="col-lg-offset-1 col-lg-11"> -->
                                        <?= Html::submitButton('Войти', ['class' => 'btn btn-green', 'name' => 'login-button']) ?>
                                    <!-- </div> -->
                                </div>

                                <div class="clearfix">
                                    <div class="remember skin-flat pull-left">
                                    <?= Html::checkbox('rememberMe', false, []); ?>
                                    <?= Html::label('Запомнить меня', 'rememberMe', []); ?>
                                    </div>
                                    <!--  //$form->field($model, 'rememberMe')->checkbox([
                                        //'template' => "<div class=\"remember skin-flat pull-left\">{input} {label}</div>\n",
                                        //'options' => [
//                                            'tag' => false
                                        //],
                                        // 'label' => "Запомнить меня",
                                    //])->label('Запомнить меня')  -->
                                    
                                    <div class="reset-password pull-right">
                                        <a href="/auth/request-password-reset">Забыли пароль?</a>
                                    </div>  
                                </div>
                                <!-- <form> -->
                                    <!-- <div class="form-group"> -->
                                        <!-- <input type="text" class="form-control" placeholder="E-mail адрес" /> -->
                                        <!--<span class="error">Заполните поле</span>-->
                                    <!-- </div> -->
                                    <!-- <div class="form-group"> -->
                                        <!-- <input type="password" class="form-control" placeholder="Пароль" /> -->
                                        <!--<span class="error">Заполните поле</span>-->                                        
                                    <!-- </div> -->
                                   <!--  <div class="form-group">
                                        <button type="submit" class="btn btn-green">Войти</button>
                                    </div>
                                    <div class="clearfix">
                                        <div class="remember skin-flat pull-left">
                                          <input type="checkbox" id="flat-checkbox-1">
                                          <label for="flat-checkbox-1">Запомнить меня</label>       
                                        </div>
                                        <div class="reset-password pull-right">
                                            <a href="forgot.html">Забыли пароль?</a>
                                        </div>                                                                              
                                    </div> -->
                                <!-- </form> -->
                                <?php ActiveForm::end(); ?>
                                <div class="auth-social">
                                    <p>Войти с помощью соц. сетей</p>
<!--                                    <ul class="list-inline">-->
<!--                                        <li><a class="btn btn-facebook btn-soc" href="#"><i class="fa fa-facebook-f"></i></a></li>-->
<!--                                        <li><a class="btn btn-instagram btn-soc" href="#"><i class="fa fa-instagram"></i></a></li>-->
<!--                                        <li><a class="btn btn-vk btn-soc" href="#"><i class="fa fa-vk"></i></a></li>-->
<!--                                        <li><a class="btn btn-gp btn-soc" href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!--                                    </ul>-->
                                    <?= yii\authclient\widgets\AuthChoice::widget([
                                        'baseAuthUrl' => ['site/auth'],
                                        'popupMode' => false,
                                    ]) ?>
                                </div>
                                <p class="text-center">Нет аккаунта? <a href="/auth/signup" class="text-sb">Зарегистрируйтесь бесплатно</a></p>                             
                            </div>
                        </div>          
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/. PAGE WRAPPER END -->
