<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use app\assets\AppAsset;

\app\assets\AppAsset::register($this);
unset($this->assetBundles['yii\web\JqueryAsset']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Enables or disables automatic detection of possible phone numbers in a webpage in Safari on iOS. -->
    <meta name="format-detection" content="telephone=no">   

    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width">

    <meta name="keywords" content="Бизнес каталог, каталог, каталог предприятий, казахские компании, компании Казахстана" />
    <meta name="description" content="ForLife.kz - быстрый и простой способ найти всё для жизни!" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Styles -->
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/grid.css">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/owl.theme.css">
    <link rel="stylesheet" href="/css/fontawesome-stars.css">
    <link rel="stylesheet" href="/css/jquery.smartmenus.bootstrap.css">
    <link rel="stylesheet" href="/css/icheck/all.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">      
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=cyrillic"
    
    <!-- Favicons -->
    <link rel="icon" type="image/png" href="/img/favicon.png">
    <!--[if IE]><link rel="shortcut icon" href="/img/favicon.png"/><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?= $content; ?>
    <!-- Go to Top Button -->
    <a class="go-to-top" href="#"></a>
    <!-- End Go to Top Button -->       
        

    <!-------------------------------- CONTENT ENDS HERE -------------------------------->
    
    
    <!-- MODALS BEGIN -->   
    <noindex>

    
    </noindex>
    <!--/. MODALS END -->       
    

    <!-- Placed at the end of the document so the pages load faster -->

    <script src="/js/plugins.js"></script>
    <script src="/js/scripts.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
