<?php
/**
 * @var $this yii\web\View
 * @var $page \app\models\StaticPage
 */
$this->title = $page->descr;
?>

<!-- PAGE WRAPPER BEGIN -->
<div class="page-wrapper">
    <?= \app\widgets\NavbarListWidget::widget() ?>

    <div class="margin50"></div>


    <!-- MAIN CONTENT BEGIN -->
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="sidebar">
                        <ul id="sidebar-nav">
                            <?php foreach ($allPages as $allPage): ?>
                                <li class="<?= $allPage->name === $page->name ? 'current' : ' ' ?>">
                                    <a href="/page/<?= $allPage->name ?>"><?= $allPage->descr ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9">
                    <div class="contained">
                        <div class="markdown-zone">
                            <?= str_replace('{THEME}', '/templates/forlife', stripslashes($page->template)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--/. MAIN CONTENT END -->
    <?= \app\widgets\FooterWidget::widget() ?>
</div>
