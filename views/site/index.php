<?php
/** @var $this yii\web\View */
/** @var $categories app\models\Category */
/** @var $vipBanners app\models\PartnersBanners */
/** @var $premiumBanners app\models\PartnersBanners */
/** @var $standardPosts app\models\Post title */
/** @var $apartmentPosts app\models\Post */
/** @var $autoPosts app\models\Post */
/** @var $countries app\models\Country */
/** @var $eatPosts app\models\Post */

use yii\helpers\Html;
use yii\helpers\Url;
$this->registerJs(<<< JS
    var container = $('.intro__broadcast');
    var width = container.width();
    container.height(width/4*3);
    container.html('<iframe src="http://demo73.santral.tv/live?w='+width+'&h='+parseInt(width/4*3)+'" frameborder="0" scrolling="no" width="100%" height="100%" allowfullscreen></iframe>')
JS
);

$this->title = 'Дисконтная система';
?>
<!-- PAGE WRAPPER BEGIN -->
<div class="page-wrapper">
    <!-- HEADER BEGIN -->
<!--    <header class="intro">-->
<!--        <nav class="intro-panel">-->
<!--            <div class="container-fluid">-->
<!--                <a href="/" title="Forlife" class="intro-panel__logo">Forlife</a>-->
<!--                <ul class="intro-panel__right">-->
<!--                    <li class="intro-panel__item">-->
<!--                        --><?php //if (Yii::$app->user->isGuest){
//                            echo Html::a('Войти', Url::to('/auth/login'), ['class' => 'intro-panel__link']);
//                        }
//                        else {
//                            echo Html::a('Выйти', Url::to('/auth/logout'), ['class' => 'intro-panel__link']);
//                        }
//                        ?>
<!--                </ul>-->
<!--            </div>-->
<!--        </nav>-->
<!--        <div class="intro__broadcast"></div>-->
<!--        <div class="stats intro-stats">-->
<!--            <div class="stats__widget">-->
<!--                <div class="stats__widget-text">У нас:</div>-->
<!--                --><?php //?>
<!--                <div class="stats__widget-list">-->
<!--                    --><?php //foreach ($countClients as $countClient): ?>
<!--                        <span class="stats__widget-item">--><?//= $countClient ?><!--</span>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
<!--                <div class="stats__widget-text">Потребителей</div>-->
<!--            </div>-->
<!--            <div class="stats__widget">-->
<!--                <div class="stats__widget-text">С нами:</div>-->
<!--                <div class="stats__widget-list">-->
<!--                    --><?php //foreach ($countPartners as $countPartner): ?>
<!--                        <span class="stats__widget-item">--><?//= $countPartner ?><!--</span>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
<!--                <div class="stats__widget-text">Потребителей</div>-->
<!--            </div>-->
<!--            <div class="stats__widget">-->
<!--                <div class="stats__widget-text">Мы в:</div>-->
<!--                <div class="stats__widget-list">-->
<!--                    --><?php //foreach ($countCities as $countCity): ?>
<!--                        <span class="stats__widget-item">--><?//= $countCity ?><!--</span>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
<!--                <div class="stats__widget-text">Регионах</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </header>-->
    <!--/. HEADER END -->
    <?= \app\widgets\NavbarListWidget::widget([]) ?>
    <?= \app\widgets\NavbarBarWidget::widget([]) ?>
    <?= \app\widgets\BannersWidget::widget(['vipBanners' => $vipBanners, 'premiumBanners' => $premiumBanners]) ?>
    <?= \app\widgets\ClassicListWidget::widget(['classicPosts' => $classicPosts]) ?>
    <?= \app\widgets\StandardListWidget::widget(['standardPosts' => $standardPosts]) ?>
<!--    --><?//= \app\widgets\StandardWidget::widget(['standardPosts' => $standardPosts]) ?>
<!--    --><?//= \app\widgets\FoodsWidget::widget(['foodPosts' => $eatPosts]) ?>
<!--    --><?//= \app\widgets\ApartmentPosts::widget(['apartmentPosts' => $apartmentPosts]) ?>
<!--    --><?//= \app\widgets\CarsWidget::widget(['autoPosts' => $autoPosts]) ?>

    <?= \app\widgets\CategoriesWidget::widget(['categories' => $categories]) ?>

</div>
<!--/. PAGE WRAPPER END -->

<?= \app\widgets\FooterWidget::widget() ?>