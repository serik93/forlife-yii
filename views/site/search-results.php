<?php
use yii\widgets\ListView;

$this->title = 'Результаты поиска';
?>
<?= \app\widgets\NavbarWidget::widget([]) ?>
<?= \app\widgets\NavbarListWidget::widget([]) ?>
<main class="main-content">
<!-- BEST PLACES BEGIN -->
<section class="best-places">
    <div class="container">
        <div class="section-heading">
            <div class="container"><h2>Результаты поиска</h2> <span class="sh-icon best-icon"></span></div>
        </div>
        <div class="box">
            <div class="deals-list row">
                <?php
                    echo ListView::widget( [
                        'summary' => '',
                        'dataProvider' => $dataProvider,
                        'itemView' => 'search-results-item',
                        'options' => ['tag' => false]
                    ] ); ?>

            </div>
        </div>
    </div>
</section>
<!--/. BEST PLACES END -->
</main>