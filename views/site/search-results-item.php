<?php
    $xfields = explode("||", $model->xfields);
    $xfieldsArray = [];
    foreach ($xfields as $xfield) {
        $str = explode("|", $xfield);
        $xfieldsArray[$str[0]] = $str[1];
    }
    ?>
<?php if (isset($xfieldsArray['logo'])): ?>
    <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="item">
            <?php if (isset($xfieldsArray['classic'])): ?><span class="deals-promo">-<?= $xfieldsArray['classic'] ?>%</span><?php endif; ?>
            <img src="<?= isset($xfieldsArray['logo']) ? $xfieldsArray['logo'] : '' ?>" class="img-responsive">
            <a href="/standard/<?= $model->alt_name ?>" class="btn">Перейти</a>
        </div>
    </div>
<?php endif; ?>