<?php
/** @var $this yii\web\View */
/** @var $categories app\models\Category */
/** @var $vipBanners array app\models\PartnersBanners */
/** @var $premiumBanners array app\models\PartnersBanners */
/** @var $standardPosts array app\models\Post */
/** @var $eatPosts array app\models\Post */
/** @var $apartmentPosts array app\models\Post */
/** @var $autoPosts array app\models\Post */
/** @var $countries array app\models\Country title */
/** @var $this yii\web\View */
use yii\widgets\Breadcrumbs;
use \yii\helpers\Url;

$this->title = $region;
//if (isset($currentCategory->single))
//    $this->params['breadcrumbs'][] = ['label' => $currentCategory->single->name, 'url' => Url::to($currentCategory->alt_name)];
//$this->params['breadcrumbs'][] = ['label' => $this->title];

$totalCount = count($vipBanners)+count($premiumBanners)+count($standardPosts)+count($eatPosts)+count($autoPosts)+count($apartmentPosts);
?>
    <!-- PAGE WRAPPER BEGIN -->
    <div class="page-wrapper">
        <?= \app\widgets\NavbarListWidget::widget([]) ?>
        <!-- MAIN CONTENT BEGIN -->
        <main class="main-content">
            <div class="page-heading">
                <div class="container">
                    <?php // Breadcrumbs::widget([
                        // 'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        // 'options' => ['class' => 'breadcrumbs clearfix']
                    // ]);
                    ?>
                    <h1><?= $region .' ('. $totalCount.')'  ?></h1>
                </div>
            </div>
            <?= \app\widgets\BannersWidget::widget(['vipBanners' => $vipBanners, 'premiumBanners' => $premiumBanners]) ?>
            <?= \app\widgets\ClassicListWidget::widget(['classicPosts' => $classicPosts]) ?>
            <?= \app\widgets\StandardListWidget::widget(['standardPosts' => $standardPosts]) ?>
<!--            --><?//= \app\widgets\StandardWidget::widget(['standardPosts' => $standardPosts]) ?>
<!--            --><?//= \app\widgets\FoodsWidget::widget(['foodPosts' => $eatPosts]) ?>
<!--            --><?//= \app\widgets\ApartmentPosts::widget(['apartmentPosts' => $apartmentPosts]) ?>
<!--            --><?//= \app\widgets\CarsWidget::widget(['autoPosts' => $autoPosts]) ?>
        </main>
        <!--/. MAIN CONTENT END -->
    </div>
    <!--/. PAGE WRAPPER END -->
<?= \app\widgets\FooterWidget::widget() ?>