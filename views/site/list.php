<?php
/** @var $this yii\web\View */
/** @var $categories array app\models\Category */
/** @var $vipBanners array app\models\PartnersBanners */
/** @var $premiumBanners array app\models\PartnersBanners */
/** @var $standardPosts array app\models\Post */
/** @var $classicPosts array app\models\Post */
/** @var $countries array app\models\Country title */
/** @var $currentCategory array app\models\Category */
/** @var $guestPosts array app\models\Category */
/** @var $recommendPosts array app\models\Category */
/** @var $cardPosts array app\models\PartnersBanners */
/** @var $this yii\web\View */
use yii\widgets\Breadcrumbs;
use \yii\helpers\Url;

$this->title = $currentCategory->name;
if (isset($currentCategory->single))
    $this->params['breadcrumbs'][] = ['label' => $currentCategory->single->name, 'url' => Url::to($currentCategory->alt_name)];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'class' => 'active'];
$totalCount = count($vipBanners)+count($premiumBanners)+count($standardPosts)+count($classicPosts)+count($guestPosts);
?>
<!-- PAGE WRAPPER BEGIN -->
<div class="page-wrapper">
    <?= \app\widgets\NavbarListWidget::widget([]) ?>
    <!-- MAIN CONTENT BEGIN -->
    <main class="main-content">
        <div class="page-heading">
            <div class="container">
                <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'options' => ['class' => 'breadcrumbs clearfix']
                    ]);
                ?>
                <h1><?= $currentCategory->name .' ('. $totalCount.')'  ?></h1>
            </div>
        </div>
        <?= \app\widgets\BannersWidget::widget(['vipBanners' => $vipBanners, 'premiumBanners' => $premiumBanners]) ?>
        <?= \app\widgets\ClassicListWidget::widget(['classicPosts' => $classicPosts]) ?>
        <?= \app\widgets\StandardListWidget::widget(['standardPosts' => $standardPosts]) ?>
        <?= \app\widgets\GuestPostsWidget::widget(['guestPosts' => $guestPosts]) ?>
        <?php // echo \app\widgets\RecommendationsWidget::widget(['recommendPosts' => $recommendationPosts]) ?>
        <?php // echo \app\widgets\CardsWidget::widget(['cardBanners' => $cardPosts]) ?>
    </main>
    <!--/. MAIN CONTENT END -->
</div>
<!--/. PAGE WRAPPER END -->
<?= \app\widgets\FooterWidget::widget() ?>