<?php
/** @var $this yii\web\View */
/** @var $model app\models\Post */

use yii\helpers\ArrayHelper;
use app\models\Category;
$this->title = 'Добавить компанию';
$this->registerCssFile('/css/selectize.css');

$this->registerCss(<<< CSS
    .loader {
        border: 4px solid #f3f3f3; /* Light grey */
        border-top: 4px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 30px;
        height: 30px;
        animation: spin 2s linear infinite;
        position: absolute;
        right: -30px;
        top: 5px;
    }
    
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .form-group .help-block {
        display: block!important;
    }
CSS
);

$this->registerJs(<<< JS
    ajaxRequest('post-category', 'post-subcategories');
    ajaxRequest('post-country', 'post-city');
    $('#post-category').change(function() {
        ajaxRequest('post-category', 'post-subcategories');
    });
    $('#post-country').change(function() {
        ajaxRequest('post-country', 'post-city');
    });
    $('#post-city').change(function() {
        ajaxRequest('post-city', 'post-district');
    });
    
    $(".file").change(function(){
        readURL(this);
    });
    
    $('.remove-btn').click(function(e) {
        e.preventDefault();
        $(this).parent().hide();
        $(this).parent().prev().show();
        $(this).parent().find('img').remove();
    });
    
    $('.skin-flat input').on('input', function(data) {
        var value = data.currentTarget.value;
        $('[name=total]').val(value * $(this).data('value'));
        $('#'+($(this).prev().attr('for'))).iCheck('check');
    });
    
    $('input[type=checkbox]').on('ifChecked', function() {
        var context = this;
        $('input[type=checkbox]').each(function() {
            if (this.id !== context.id) {
                $('input#'+this.id).iCheck('uncheck');
            }
        });
    });
    
    
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $(input).parent().next().prepend('<img src="'+e.target.result+'" />');
                $(input).parent().hide();
                $(input).parent().next().show();
            };
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function ajaxRequest(parentSelector, selector) {
        setLoader('#'+parentSelector);
        var jquerySelector = $('#'+selector);
        var selectize = jquerySelector[0].selectize;
        selectize.clearOptions();
        selectize.clear();
        $.ajax({
            url: 'get-'+selector+'?id='+$('#'+parentSelector).val(),
            method: 'GET',
            success: function(data) {
                var dict = [];
                for (var i in data) {
                    dict.push({value:i, text:data[i]});
                    selectize.addItem(i);
                }
                selectize.addOption(dict);
                
                unsetLoader('#'+parentSelector);
            }
        });
    }
    function setLoader(selector) {
        $(selector).next().after('<div class="loader"></div>');
    }
    function unsetLoader(selector) {
        $(selector).next().next().remove();
    }
    $('input').on('input', function(e) {
        if ($(this).val().length > 70)
            $(this).val($(this).val().slice(0, 70));
      $(this).next().next().find('span').text(70 - $(this).val().length);
    })
JS
);

?>

<!-- PAGE WRAPPER BEGIN -->
<div class="page-wrapper">


    <?= \app\widgets\NavbarListWidget::widget() ?>

    <div class="margin30"></div>

    <!-- MAIN CONTENT BEGIN -->
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="contained">
                        <div class="new-advert">
                            <?php if (Yii::$app->session->hasFlash('success')) {
                                echo '<p>'.(Yii::$app->session->getFlash('success')).'</p>';
                            } ?>
                            <h4>Подать новое объявление</h4>
                            <?php $form = \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'fieldConfig' => ['options' => ['tag' => false]]]) ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Тип объявления*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select class="selectize" id="type">
                                                <option value="0">Платное</option>
                                                <option value="1">Бесплатное</option>
                                            </select>
                                            <span class="help-text">Выберите тип объявления</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>№ Дисконтной карты:*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'discount_card')->textInput(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Осталось <span class="number">70</span> символов</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Имя владельца:*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'owner_name')->textInput(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Осталось <span class="number">70</span> символов</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Название компании:*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'title')->textInput(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Осталось <span class="number">70</span> символов</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Название бренда:*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'brand_name')->textInput(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Осталось <span class="number">70</span> символов</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Логотип компаний:*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <label class="upload-img">
                                                    <?= $form->field($model, 'logo')->fileInput(['class' => 'hidden file'])->label(false) ?>
<!--                                                    <input type='file' class="hidden file" id="post-logo" />-->
                                                </label>
                                                <div class="upload-img img-uploaded" style="display: none">
                                                    <button class="remove-btn">✕</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Логотип бренда:*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <label class="upload-img">
                                                    <?= $form->field($model, 'brand')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                </label>
                                                <div class="upload-img img-uploaded" style="display: none">
                                                    <button class="remove-btn">✕</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Категория*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'category')->dropDownList(ArrayHelper::map(Category::findAll(['parentid' => 0]), 'id', 'name'), ['class' => 'selectize'])->label(false) ?>
                                            <span class="help-text">Выберите Категорию</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Подкатегория*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'subcategories')->dropDownList([], ['class' => 'selectize'])->label(false) ?>
                                            <span class="help-text">Выберите Подкатегорию</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Короткое описание*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'short_story')->textarea(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Осталось 150 символов</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Полное описание*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'full_story')->textarea(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Осталось 150 символов</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Тип объявления*</label>
                                        </div>
                                        <div class="col-md-9" id="calc">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-0">Start Banner 300x300</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-0">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-0"> 1 мес./ 0 тг.</label>
                                                        <input type="text" data-value="0" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-1">Vip Banner 750x90</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-1">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-1"> 1 мес./30 000 тг.</label>
                                                        <input type="text" data-value="30000" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-2">Premium Banner 300x90</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-2">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-2"> 1 мес./18 000 тг.</label>
                                                        <input type="text" data-value="18000" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-3">Classic Banner 300x300</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-3">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-3"> 1 мес./12 000 тг.</label>
                                                        <input type="text" data-value="12000" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-4">Special Banner 200x200</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-4">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-4"> 1 мес./24 000 тг.</label>
                                                        <input type="text" data-value="24000" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-5">Standard Banner 300x184</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-5">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-5"> 1 мес./10 000 тг.</label>
                                                        <input type="text" data-value="10000" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-6">Best Banner 300x184</label>
                                                        <input type="checkbox" name="checkbox" id="flat-checkbox-6">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label for="flat-checkbox-6"> 1 мес./5 000 тг.</label>
                                                        <input type="text" data-value="5000" class="form-control">
                                                        <span>мес.</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <div class="skin-flat">
                                                        <label>Итого:</label>
                                                        <input class="form-control" name="total" style="width: 100px">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <span class="help-text"> Выберите нужный себе месяц.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Страна*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'country')->dropDownList(ArrayHelper::map(\app\models\Country::find()->all(), 'id', 'name'), ['class' => 'selectize'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Город*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'city')->dropDownList([],  ['class' => 'selectize'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Район*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'district')->dropDownList([],  ['class' => 'selectize'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Адрес</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'address')->textInput(['class' => 'form-control'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Телефон*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'phone')->textInput(['class' => 'form-control'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Email*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'email')->textInput(['class' => 'form-control'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Фото*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" data-gutter="10">
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <label class="upload-img">
                                                        <?= $form->field($model, 'img1')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                    </label>
                                                    <div class="upload-img img-uploaded" style="display: none">
                                                        <button class="remove-btn">✕</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <label class="upload-img">
                                                        <?= $form->field($model, 'img2')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                    </label>
                                                    <div class="upload-img img-uploaded" style="display: none">
                                                        <button class="remove-btn">✕</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <label class="upload-img">
                                                        <?= $form->field($model, 'img3')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                    </label>
                                                    <div class="upload-img img-uploaded" style="display: none">
                                                        <button class="remove-btn">✕</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <label class="upload-img">
                                                        <?= $form->field($model, 'img4')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                    </label>
                                                    <div class="upload-img img-uploaded" style="display: none">
                                                        <button class="remove-btn">✕</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <label class="upload-img">
                                                        <?= $form->field($model, 'img5')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                    </label>
                                                    <div class="upload-img img-uploaded" style="display: none">
                                                        <button class="remove-btn">✕</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <label class="upload-img">
                                                        <?= $form->field($model, 'img6')->fileInput(['class' => 'hidden file'])->label(false) ?>
                                                    </label>
                                                    <div class="upload-img img-uploaded" style="display: none">
                                                        <button class="remove-btn">✕</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="help-text">С фотографиями увеличивается вероятность того, что вашу вещь найдут.<span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Видео*</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?= $form->field($model, 'vidos')->textInput(['class' => 'form-control'])->label(false) ?>
                                            <span class="help-text">Пример: youtube.com/watch?v=ONLQoVkqlg4<span>
                                        </div>
                                    </div>
                                </div>
                                <p class="advert-agree">Нажимая кнопку «Добавить объявление», Вы принимаете условия <a href="#">Пользовательского соглашения.</a></p>
                                <button type="submit" class="btn">Добавить объявление</button>
                            <?php \yii\widgets\ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="sidebar">
                        <div class="form-suggest warning">
                            <h4>Банеры</h4>
                            <img src="/img/banners.png" class="img-responsive">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--/. MAIN CONTENT END -->


</div>
<!--/. PAGE WRAPPER END -->
<?= \app\widgets\FooterWidget::widget() ?>