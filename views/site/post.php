<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\Post
 */

$this->registerCssFile('/css/lightgallery.css');
$this->registerCssFile('/css/magnific-popup.css');

$this->title = stripslashes($model->title);
?>

<!-- PAGE WRAPPER BEGIN -->
<div class="page-wrapper">
    <?= \app\widgets\NavbarListWidget::widget([]) ?>

    <!-- MAIN CONTENT BEGIN -->
    <main class="main-content">
        <div class="container">
            <!-- place images -->
            <div class="place-images-container">
                <div class="place-images place-gallery owl-carousel lightgallery owl-loaded owl-drag">
                    <?php
                    $xfields = explode("||", $model->xfields);
                    $xfieldsArray = [];
                    foreach ($xfields as $xfield) {
                        $str = explode("|", $xfield);
                        $xfieldsArray[$str[0]] = $str[1];
                    }
                    if (isset($xfieldsArray['images'])) {
                        $doc = new DOMDocument();
                        libxml_use_internal_errors(true);
                        $doc->loadHTML($xfieldsArray['images']);
                        libxml_clear_errors();
                        // read all image tags into an array
                        $imageTags = $doc->getElementsByTagName('img');
                        foreach($imageTags as $tag) {
                            echo "<a class='place-image' href='' data-sub-html='<p>Forlife</p>' data-src=".stripslashes($tag->getAttribute('src'))."><img src=".stripslashes($tag->getAttribute('src'))." /></a>";
                        }
                    }
                    ?>
                </div>
            </div>
            <!--/. place images -->
        </div>

        <!-- PLACE SINGLE BEGIN -->
        <div class="container">
            <div class="box place-heading">
                <div class="place-heading-top box-body clearfix">
                    <h1 class="place-title"><?= $this->title ?></h1>
                    <div class="place-main-img" style="background-image: url('<?= isset($xfieldsArray['logo']) ? $xfieldsArray['logo'] : '' ?>')"></div>
                    <div class="place-services">
                        <span><?= stripslashes($model->short_story) ?></span>
                    </div>
                    <div class="place-rating">
                        <span class="rating rating-lg"><i class="fa fa-star"></i>5.0 / <a href="#">1</a></span>
                        <span class="pre-d"><?php if (isset($xfieldsArray['price'])) echo $xfieldsArray['price'] ?></span>
                    </div>
                    <div class="place-heading-links">
                        <div class="place-promote-dropdown">
                            <div class="btn-group">
<!--                                <a href="#" class="btn"><i class="icon icon-promote"></i> Поднять заведение</a>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-divider clearfix"></div>
                <div class="place-pages-links box-body clearfix">
                    <div class="place-pages pages-nav nav nav-tab">
                        <ul>
                            <li class="active">
                                <a href="#desc" data-toggle="tab">Описание</a>
                            </li>
                            <li>
                                <a href="#reviews" data-toggle="tab">Отзывы <small><?= \yii2mod\comments\models\CommentModel::find()->where(['entityId' => $model->id, 'status' => 1])->count('1') ?></small></a>
                            </li>
                            <li>
                                <a href="#gallery" data-toggle="tab">Фото и видео <small><?= isset($imageTags) ? $imageTags->length: 0 ?></small></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="box place-bottom">
                        <div class="box-body clearfix">
                            <div class="tab-content">
                                <div id="desc" class="tab-pane fade in active">
                                    <div class="place-desc"><?= stripslashes($model->full_story) ?><br/>
                                        <?php if (isset($xfieldsArray['www'])): ?>
                                            <?php foreach (explode(' ', $xfieldsArray['www']) as $www): ?>
                                                <a href="http://<?= $www ?>"><?= $www ?></a>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div id="reviews" class="tab-pane fade">
                                    <div class="place-desc">
                                        <?php echo \yii2mod\comments\widgets\Comment::widget([
                                            'model' => $model,
                                        ]); ?>
                                    </div>
                                </div>
                                <div id="gallery" class="tab-pane fade">
                                    <ul class="lightgallery row" data-gutter="10">
                                    <?php
                                    if (isset($xfieldsArray['images'])) {
                                        $doc = new DOMDocument();
                                        $doc->loadHTML($xfieldsArray['images']);
                                        $imageTags = $doc->getElementsByTagName('img');
                                        foreach($imageTags as $tag) {
                                            echo "<li class='col-md-4 col-sm-4' data-src=".stripslashes($tag->getAttribute('src')).">
                                                       <div class='place-photo'>
                                                        <img src=".stripslashes($tag->getAttribute('src'))." />
                                                       </div>
                                                   </li>";
                                        }
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="box-divider clearfix"></div>
                        <div class="box-body clearfix">
                            <div class="place-desc-options">
                                <div class="row">
                                    <label class="col-md-4 col-sm-4">Просмотров</label>
                                    <p class="col-md-8 col-sm-8"><?= $model->extras->news_read ?></p>
                                </div>
                                <?php if (isset($xfieldsArray['classic'])): ?>
                                    <div class="row">
                                        <label class="col-md-4 col-sm-4">Скидка</label>
                                        <p class="col-md-8 col-sm-8"><?= $xfieldsArray['classic'] ?></p>
                                    </div>
                                <?php elseif (isset($xfieldsArray['vip'])): ?>
                                    <div class="row">
                                        <label class="col-md-4 col-sm-4">Скидка</label>
                                        <p class="col-md-8 col-sm-8"><?= $xfieldsArray['vip'] ?></p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
<!--                    </div>-->
                    <div class="box place-bottom place-info">
                        <div class="box-body clearfix">
                            <div class="map">
                                <script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
                                <script type="text/javascript">
                                    ymaps.ready(init);
                                    function init() {
                                        myMapGeo = new ymaps.Map('YMapsIDgeopoint', {
                                            center: [55.600852, 36.46187],
                                            zoom: 14,
                                            controls: ['zoomControl', 'searchControl', 'typeSelector', 'geolocationControl']
                                        });
                                        myMapGeo.events.add('click', function (e) {
                                            var coords = e.get('coords');
                                            $('#geopoint').val( [coords[0].toPrecision(6), coords[1].toPrecision(6)].join(', ') );
                                            myMapGeo.balloon.open(coords, 'Местонахождение отмечено!<br><sup>Вы можете закрыть и выбрать другое местоположение.</sup>');
                                        });
                                    }
                                </script>
                                <div id="YMapsIDgeopoint" style="width:100%; height: 100%;"></div>
                                <input type="text" id="geopoint" name="geopoint" class="hidden" placeholder="GPS координаты" value="" maxlength="80" size="65" class="">
                            </div>
                            <div class="place-info-list">
                                <ul class="row">
                                    <li class="col-md-4 col-sm-4">
                                        <div class="place-info-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="place-info-text">
                                            <?= stripslashes($xfieldsArray['address']) ?>
                                        </div>
                                    </li>
                                    <li class="col-md-4 col-sm-4 place-open">
                                        <div class="place-info-icon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <?php if (isset($xfieldsArray['work_graphic'])): ?>
                                            <div class="place-info-text">
                                                <!--<div class="schedule-status">Сейчас открыто</div>-->
                                                <div class="schedule-list"><?= isset($xfieldsArray['work_graphic']) ? $xfieldsArray['work_graphic'] : '' ?></div>
                                            </div>
                                        <?php endif; ?>
                                    </li>
                                    <li class="col-md-4 col-sm-4">
                                        <div class="place-info-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="place-info-text">
                                            <?php if (isset($xfieldsArray['phone'])): ?>
                                                <?php foreach (explode('__NEWL__',$xfieldsArray['phone']) as $phone): ?>
                                                    <p><?= $phone ?></p>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?= \app\widgets\RelatedPostWidget::widget(['relatedPosts' => $relatedPosts]) ?>
            </div>
        </div>
        <!--/. PLACE SINGLE END -->


        <?= \app\widgets\CategoriesWidget::widget(['categories' => $categories]) ?>



    </main>
    <!--/. MAIN CONTENT END -->


</div>
<!--/. PAGE WRAPPER END -->

<?= \app\widgets\FooterWidget::widget() ?>
