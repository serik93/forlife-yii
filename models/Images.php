<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%images}}".
 *
 * @property string $id
 * @property string $images
 * @property integer $news_id
 * @property string $author
 * @property string $date
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['images'], 'required'],
            [['images'], 'string'],
            [['news_id'], 'integer'],
            [['author'], 'string', 'max' => 40],
            [['date'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'images' => 'Images',
            'news_id' => 'News ID',
            'author' => 'Author',
            'date' => 'Date',
        ];
    }
}
