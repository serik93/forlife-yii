<?php

namespace app\models;

use ReflectionClass;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property string $autor
 * @property string $date
 * @property string $short_story
 * @property string $full_story
 * @property string $xfields
 * @property string $title
 * @property string $descr
 * @property string $keywords
 * @property string $category
 * @property string $alt_name
 * @property string $comm_num
 * @property integer $allow_comm
 * @property integer $allow_main
 * @property integer $approve
 * @property integer $fixed
 * @property integer $allow_br
 * @property string $symbol
 * @property string $tags
 * @property string $metatitle
 * @property string $city
 * @property string $logo
 */
class Post extends \yii\db\ActiveRecord
{
    public $q;

    public $discount_card;
    public $owner_name;
    public $brand_name;
    public $phone;
    public $email;
    public $vidos;
    public $country;
    public $district;
    public $subcategories;
    public $brand;
    public $img1;
    public $img2;
    public $img3;
    public $img4;
    public $img5;
    public $img6;
    public $address;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'q', 'discount_card', 'owner_name', 'brand_name', 'phone', 'email', 'vidos',
            'img1', 'img2', 'img3', 'img4', 'img5', 'img6', 'brand', 'address' ], 'safe'],
            [['short_story', 'full_story', 'title', 'country'], 'required'],
            [['short_story', 'full_story', 'xfields', 'keywords'], 'string'],
            [['comm_num', 'allow_comm', 'allow_main', 'approve', 'fixed', 'allow_br'], 'integer'],
            [['autor'], 'string', 'max' => 40],
            [['title', 'tags', 'metatitle', 'city', 'logo'], 'string', 'max' => 255],
            [['descr', 'category', 'alt_name'], 'string', 'max' => 200],
            [['symbol'], 'string', 'max' => 3],
            //[['logo'], 'file','extensions' => 'jpg, jpeg, png, bmp, jpe'],
            [['logo', 'brand', 'img1', 'img2', 'img3', 'img4', 'img5', 'img6'], 'file','extensions' => 'jpg, jpeg, png, bmp, jpe', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'autor' => 'Autor',
            'date' => 'Date',
            'short_story' => 'Short Story',
            'full_story' => 'Full Story',
            'xfields' => 'Xfields',
            'title' => 'Title',
            'descr' => 'Descr',
            'keywords' => 'Keywords',
            'category' => 'Category',
            'alt_name' => 'Alt Name',
            'comm_num' => 'Comm Num',
            'allow_comm' => 'Allow Comm',
            'allow_main' => 'Allow Main',
            'approve' => 'Approve',
            'fixed' => 'Fixed',
            'allow_br' => 'Allow Br',
            'symbol' => 'Symbol',
            'tags' => 'Tags',
            'metatitle' => 'Metatitle',
            'city' => 'City',
            'logo' => 'Лого',
        ];
    }

    public function getExtras()
    {
        return $this->hasOne(PostExtras::className(), ['news_id' => 'id']);
    }

    /**
     * @param City|Country $region
     * @param array|ActiveQuery $categories
     * @param integer $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getStandardPosts($region, $categories, $limit)
    {
        $categoryIds = [];
        if (isset($categories)) {
            foreach ($categories as $category) {
                $categoryIds[] = $category->id;
                foreach ($category->parent as $category) {
                    $categoryIds[] = $category->id;
                }
            }
        }

        $query = self::find()->where(['approve' => 1])->andWhere(['like', 'xfields', '%top|top2%', false])->orderBy('id DESC')->limit($limit);
        if (isset($region))
            $query->andWhere(['like', 'xfields', '%'.$region->name.'%', false]);
        if (isset($category))
            $query->andWhere(['REGEXP', 'category', implode('|', $categoryIds)]);
        return $query->all();
    }

    /**
     * @param Country|City $region
     * @param Category $currentCategory
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getGuestPosts($region, $currentCategory)
    {
        $query = self::find()->where(['approve' => 1])->andWhere(['category' => $currentCategory->id])->andWhere(['autor' => 'guest']);
        if (isset($region))
            $query->andWhere(['like', 'xfields', '%'.$region->name.'%', false]);
        return $query->all();
    }

    /**
     * @param Country|City $region
     * @param Category[] $categories
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getClassicPosts($region, $categories)
    {
        $categoryIds = [];
        if (isset($categories)) {
            foreach ($categories as $category) {
                $categoryIds[] = $category->id;
                foreach ($category->parent as $category) {
                    $categoryIds[] = $category->id;
                }
            }
        }

        $query = self::find()->where(['approve' => 1])->andWhere(['like', 'xfields', '%top|top1%', false])->limit(50);
        if (isset($region))
            $query->andWhere(['like', 'xfields', '%'.$region->name.'%', false]);
        if (isset($categories))
            $query->andWhere(['REGEXP', 'category', implode('|', $categoryIds)]);
        return $query->all();
    }


    public function beforeSave($insert)
    {
        // country|Казахстан||city|Тараз||address|ул. Абая, 121||phone|+7 (7262) 43 64 65__NEWL__+7 (777) 547 77 56||email|roden-ph@mail.ru||www|www.roden.kz||top|top1||vip|10||images|<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759252_1.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759262_2.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759211_3.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759224_4.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759292_5.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759258_6.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759261_7.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759237_8.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759293_9.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759282_10.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759235_11.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759273_12.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759249_13.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759267_14.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759282_15.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759287_16.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759275_17.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759285_18.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759307_19.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759311_20.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759344_21.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759331_22.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759326_23.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759298_24.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759327_25.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759363_26.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759279_27.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759311_28.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759335_29.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759287_30.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759375_31.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759373_32.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759367_33.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759378_34.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759322_35.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759316_36.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759335_37.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759314_38.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759394_39.png\" style=\"float:left;\" /><br />__NEWL__<img src=\"http://forlife.kz/uploads/posts/2017-03/1489759395_40.png\" style=\"float:left;\" />
        // country|Казахстан||city|Алматы||address|г. Алматы </br>Пушкин к-сі, 36, </br>ТД \"Мегатау\", 408-офис||phone|+7 702 60 66 555||www|qazaqkumis.com||top|top1||logo|http://forlife.kz/uploads/posts/2017-05/1496054076_classic-banner-240x240.png||work_graphic|Понедельник - Суббота </br>10:00 - 19:00
        // city|5||email|test@test.kz||logo|http://forlife.kz/uploads/guest/1497815448_afisha.jpg||brand|http://forlife.kz/uploads/guest/1497815448_arenda.jpg
        $data = [];
        $images = [];
        if (!empty($this->country)) {
            $country = Country::findOne($this->country);
            $data[] = 'country|' . (isset($country) ? $country->name : '');
        }

        if (!empty($this->city)) {
            $city = City::findOne($this->city);
            $data[] = 'city|' . (isset($city) ? $city->name : '');
        }

        if (!empty($this->district)) {
            $district = District::findOne($this->district);
            $data[] = 'district|' . (isset($district) ? $district->name : '');
        }

        if (!empty($this->discount_card))
            $data[] = 'disctount_card|'. $this->discount_card;

        if (!empty($this->owner_name))
            $data[] = 'owner_name|' . $this->owner_name;

        if (!empty($this->brand_name))
            $data[] = 'brand_name|' . $this->brand_name;

        if (!empty($this->vidos))
            $data[] = 'vidos|' . $this->vidos;

        if (!empty($this->address))
            $data[] = 'address|'.$this->address;

        if (!empty($this->phone))
            $data[] = 'phone|'.$this->phone;

        if (!empty($this->email))
            $data[] = 'email|'.$this->email;

        if (!file_exists(__DIR__.'/../uploads/posts/'.date('Y-m'))) {
            mkdir(__DIR__.'/../uploads/posts/'.date('Y-m'), 0777, true);
        }

        if (isset($this->logo)) {
            $this->logo = UploadedFile::getInstance($this, 'logo');
            if (isset($this->logo)) {
                $this->logo->saveAs(__DIR__ . '/../uploads/posts/' . date('Y-m') . '/' . ((string)time()) . '_' . $this->logo->baseName . '.' . $this->logo->extension);
                $data[] = 'logo|http://forlife.kz/uploads/posts/' . date('Y-m') . '/' . ((string)time()) . '_' . $this->logo->baseName . '.' . $this->logo->extension;
            }
        }
        if (isset($this->brand)) {
            $this->brand = UploadedFile::getInstance($this, 'brand');
            if (isset($this->brand)) {
                $this->brand->saveAs(__DIR__ . '/../uploads/posts/' . date('Y-m') . '/' . ((string)time()) . '_' . $this->brand->baseName . '.' . $this->brand->extension);
                $data[] = 'brand|http://forlife.kz/uploads/posts/' . date('Y-m') . '/' . ((string)time()) . '_' . $this->brand->baseName . '.' . $this->brand->extension;
            }
        }
        for ($i = 1; $i < 7; $i++) {
            $var = 'img'.$i;
            if (!empty($this->$var)) {
                $this->$var = UploadedFile::getInstance($this, ('img'.$i));
                if (!empty($this->$var)) {
                    $this->$var->saveAs(__DIR__ . '/../uploads/posts/' . date('Y-m') . '/' . ((string)time()) . '_' . $this->$var->baseName . '.' . $this->$var->extension);
                    $images[] = '<img src="http://forlife.kz/uploads/posts/' . date('Y-m') . '/' . ((string)time()) . '_' . $this->$var->baseName . '.' . $this->$var->extension . '" />';
                }
            }
        }

        $this->xfields = implode('||', $data).(count($images) > 0 ? implode('__NEWL__', $images) : '');
        $this->keywords = 'not set';
        $this->autor = 'guest';
        $this->date = date('Y-m-d H:i:s');
        $this->alt_name = strtolower(str_replace(' ', '', strstr(strtoupper($this->short_story), 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЫЬЪЧШЩЭЮЯ ', 'ABBGDEEJZIIKLMNOPRSTUFHY  CSSEUA')));
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $allImages = [];
        $postExtras = new PostExtras();
        $postExtras->user_id = 5162;
        $postExtras->news_id = $this->id;
        $postExtras->allow_rate = 1;
        $postExtras->news_read = 0;
        $postExtras->rating = 0;
        $postExtras->save();

        $xfields = explode("||", $this->xfields);
        $xfieldsArray = [];
        foreach ($xfields as $xfield) {
            $str = explode("|", $xfield);
            $xfieldsArray[$str[0]] = $str[1];
        }
        $logo = isset($xfieldsArray['logo']) ? $xfieldsArray['logo'] : '';
        $logo = str_replace('http://forlife.kz/uploads/posts/', '', $logo);
        if (!empty($logo))
            $allImages[] = $logo;

        $brand = isset($xfieldsArray['brand']) ? $xfieldsArray['brand'] : '';
        $brand = str_replace('http://forlife.kz/uploads/posts/', '', $brand);
        if (!empty($brand))
            $allImages[] = $brand;

        $imagesArray = [];

        for ($i = 1; $i < 7; $i++) {
            if (isset($xfieldsArray['img'.$i])) {
                if (isset($xfieldsArray['img' . $i])) {
                    $img = $xfieldsArray['img' . $i];
                    $imagesArray[] = str_replace('http://forlife.kz/uploads/posts/', '', $img);
                }
            }
        }
        if (count($imagesArray) > 0) {
            $allImages = ArrayHelper::merge($allImages, $imagesArray);
        }

        $images = new Images();
        $images->images = implode('|||', $allImages);
        $images->news_id = $this->id;
        $images->author = 'guest';
        $images->date = (string)time();
        $images->save();
    }
}