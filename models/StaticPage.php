<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.06.2017
 * Time: 13:27
 */

namespace app\models;


use yii\db\ActiveRecord;

class StaticPage extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%static}}';
    }


}