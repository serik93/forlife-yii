<?php

namespace app\models;

use ReflectionClass;
use Yii;

/**
 * This is the model class for table "{{%partners_banners}}".
 *
 * @property integer $id
 * @property string $link
 * @property string $text
 * @property string $group
 * @property integer $group_id
 * @property integer $status
 * @property integer $sort
 * @property string $name
 * @property string $alt_name
 * @property string $type
 * @property string $preview
 * @property string $click
 * @property string $date_preview
 * @property string $users
 * @property string $date
 * @property string $target
 * @property string $many_img
 * @property string $count_click
 * @property string $count_preview
 * @property string $navigator
 * @property string $cat
 * @property string $session
 * @property string $all_preview
 * @property string $template
 * @property string $geo
 * @property string $other
 * @property integer $country
 * @property integer $city
 * @property integer $raion
 */
class PartnersBanners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partners_banners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'text', 'date', 'navigator', 'cat', 'all_preview', 'geo', 'other'], 'string'],
            [['group_id', 'status', 'sort', 'country', 'city', 'raion'], 'integer'],
            [['name', 'alt_name', 'type', 'preview', 'click', 'date_preview', 'users', 'date', 'target', 'many_img', 'navigator', 'cat', 'session', 'all_preview', 'country', 'city', 'raion'], 'required'],
            [['group', 'name', 'alt_name', 'type', 'preview', 'click', 'date_preview', 'users', 'target', 'count_click', 'count_preview', 'session', 'template'], 'string', 'max' => 255],
            [['many_img'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'text' => 'Text',
            'group' => 'Group',
            'group_id' => 'Group ID',
            'status' => 'Status',
            'sort' => 'Sort',
            'name' => 'Name',
            'alt_name' => 'Alt Name',
            'type' => 'Type',
            'preview' => 'Preview',
            'click' => 'Click',
            'date_preview' => 'Date Preview',
            'users' => 'Users',
            'date' => 'Date',
            'target' => 'Target',
            'many_img' => 'Many Img',
            'count_click' => 'Count Click',
            'count_preview' => 'Count Preview',
            'navigator' => 'Navigator',
            'cat' => 'Cat',
            'session' => 'Session',
            'all_preview' => 'All Preview',
            'template' => 'Template',
            'geo' => 'Geo',
            'other' => 'Other',
            'country' => 'Country',
            'city' => 'City',
            'raion' => 'Raion',
        ];
    }

    /**
     * @param Country|City $region
     * @param Category|null $category
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getVipBanners($region, $category = null)
    {
        $query = self::find()->where(['group' => '90x1000'])->limit(5)->orderBy('id DESC');
        //var_dump($region->id - 1);die();
        if (isset($region))
            $query->andWhere([strtolower((new ReflectionClass($region))->getShortName()) => $region->id - 1]);
        if (isset($category))
            $query->andWhere(['in', 'cat', ['all', $category->id]]);
        return $query->all();
    }

    /**
     * @param Country|City $region
     * @param Category|null $category
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getPremiumBanners($region, $category = null)
    {
        $query = self::find()->where(['group' => 'premium'])->limit(15)->orderBy('id DESC');
        if (isset($region))
            $query->andWhere([strtolower((new ReflectionClass($region))->getShortName()) => $region->id - 1]);
        if (isset($category))
            $query->andWhere(['in', 'cat', ['all', $category->id]]);
        return $query->all();
    }
}
