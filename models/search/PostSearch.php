<?php
namespace app\models\search;

use app\models\Post;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PostSearch extends Post
{
    public $q;
    public function rules()
    {
        return [
            ['q', 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->where(['like', 'xfields', '%top|top1%', false]);
        //$query->andWhere(['like', 'xfields', '%classic%', false]);
//        $query->where(['AND', ['like', 'xfields', '%top|top1%', false], ['OR', [
//            ['like', 'title1', '%'.$this->q.'%', false],
//        ]]]);

        $query->AndFilterWhere(['like', 'LOWER(title)', '%'.strtolower($this->q).'%', false]);
//            ->orFilterWhere(['like', 'LOWER(short_story)', '%'.strtolower($this->q).'%', false])
//            ->orFilterWhere(['like', 'LOWER(full_story)', '%'.strtolower($this->q).'%', false])
//            ->orFilterWhere(['like', 'LOWER(keywords)', '%'.strtolower($this->q).'%', false])
//            ->orFilterWhere(['like', 'LOWER(descr)', '%'.strtolower($this->q).'%', false]);

        return $dataProvider;
    }
}