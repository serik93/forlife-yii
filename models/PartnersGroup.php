<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%partners_group}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $alt_name
 * @property integer $count_banners
 * @property integer $status
 * @property string $session_name
 */
class PartnersGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partners_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alt_name'], 'required'],
            [['count_banners', 'status'], 'integer'],
            [['name', 'alt_name', 'session_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alt_name' => 'Alt Name',
            'count_banners' => 'Count Banners',
            'status' => 'Status',
            'session_name' => 'Session Name',
        ];
    }
}
