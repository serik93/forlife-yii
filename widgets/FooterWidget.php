<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.05.2017
 * Time: 14:09
 */
namespace app\widgets;



use yii\base\Widget;

class FooterWidget extends Widget {
    public function run()
    {
        return $this->render('footer');
    }
}