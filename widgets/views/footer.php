<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.05.2017
 * Time: 14:03
 */
?>
<!-- FOOTER BEGIN -->
<footer class="footer">
    <div class="container">
        <div class="footer-main">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <img src="/img/logo.png">
                    <p>Интернет-портал Forlife.kz объединяет такие сайты, как Бизнес-справочник, Дисконтная система, Купонная система Казахстана и интернет-канал  TVForlife.kz. Наша основная задача– создать по-настоящему выгодную и универсальную систему взаимодействия между всеми участниками потребительского рынка.</p>
                    <ul class="footer-social list-inline">
                        <li>
                            <a href="" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="" target="_blank"><i class="fa fa-youtube"></i></a>
                        </li>
                        <li>
                            <a href="" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li>
                            <a href="" target="_blank"><i class="fa fa-vk"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="#">O Card.Forlfe.kz</a></li>
                            <li><a href="#">О Forlife Group</a></li>
                            <li><a href="#">СМИ о нас</a></li>
                            <li><a href="#">Благотворительный фонд</a></li>
                        </ul>
                        <a href="#" class="btn">Вакансии</a>
                    </div>
                    <div class="footer-widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="#">O Card.Forlfe.kz</a></li>
                            <li><a href="#">О Forlife Group</a></li>
                            <li><a href="#">СМИ о нас</a></li>
                            <li><a href="#">Благотворительный фонд</a></li>
                        </ul>
                        <a href="#" class="btn">Открыть Представительство</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="#">O Card.Forlfe.kz</a></li>
                            <li><a href="#">О Forlife Group</a></li>
                            <li><a href="#">СМИ о нас</a></li>
                            <li><a href="#">Благотворительный фонд</a></li>
                        </ul>
                        <a href="#" class="btn">Купить карту</a>
                    </div>
                    <div class="footer-widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="#">O Card.Forlfe.kz</a></li>
                            <li><a href="#">О Forlife Group</a></li>
                            <li><a href="#">СМИ о нас</a></li>
                            <li><a href="#">Благотворительный фонд</a></li>
                        </ul>
                        <a href="#" class="btn">Вакансии</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="#">O Card.Forlfe.kz</a></li>
                            <li><a href="#">О Forlife Group</a></li>
                            <li><a href="#">СМИ о нас</a></li>
                            <li><a href="#">Благотворительный фонд</a></li>
                        </ul>
                        <a href="#" class="btn">Вакансии</a>
                    </div>
                    <div class="footer-widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="#">O Card.Forlfe.kz</a></li>
                            <li><a href="#">О Forlife Group</a></li>
                            <li><a href="#">СМИ о нас</a></li>
                            <li><a href="#">Благотворительный фонд</a></li>
                        </ul>
                        <a href="#" class="btn">Вакансии</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    © Copyright 2015, Все права защищены
                </div>
                <div class="col-md-6 col-sm-6 text-right">
                    Сделано в <a href="#">Forlife.kz</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/. FOOTER END -->
