<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.05.2017
 * Time: 14:16
 * @var $categories app\models\Category
 */
?>
<!--CATEGORIES BEGIN -->
<section class="categories">
    <div class="container">
        <div class="categories-heading">Искать в категориях</div>
        <div class="row">
            <?php $counter = 0; ?>
            <?php foreach ($categories as $category): ?>
                <?php if ($counter%3 == 0): ?><div class="col-md-3 col-sm-6"><?php endif; ?>
                <div class="categories-item">
                    <h3 class="categories-title"><a href="<?= $category->alt_name ?>"><?= $category->name ?></a></h3>
                    <?php if (isset($category->parent)): $count = 0; ?>
                        <?php foreach ($category->parent as $parent): ?>
                            <?php if ($count == 0) echo '<ul class="sub-categories">';?>
                            <?php if ($count == 3) echo '<ul id="category'.$category->id.'" class="sub-categories collapse">';?>
                            <li><a href="/category/<?= $parent->alt_name ?>"><?= $parent->name ?></a></li>
                            <?php if ($count == 2 || $count == count($category->parent)-1) echo '</ul>';?>
                            <?php ++$count; ?>
                        <?php endforeach; ?>
                        <a class="subcategories-expander" data-toggle="collapse" data-parent="#accordion" data-target="#category<?= $category->id ?>" href="#category<?= $category->id ?>" data-hide-text="Скрыть" data-show-text="Показать все">Показать все</a>
                    <?php endif; ?>
                </div>
                <?php if ($counter%3 == 2): ?></div><?php endif; ?>
                <?php $counter++; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!--/. CATEGORIES END -->
