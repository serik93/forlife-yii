<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.05.2017
 * Time: 14:13
 */
namespace app\widgets;

use yii\base\Widget;

class CategoriesWidget extends Widget{

    public $categories;

    public function run()
    {
        return $this->render('categories_bottom', [
            'categories' => $this->categories
        ]);
    }
}