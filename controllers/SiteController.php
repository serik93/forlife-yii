<?php

namespace app\controllers;

use app\components\AuthHandler;
use app\models\Category;
use app\models\City;
use app\models\Country;
use app\models\District;
use app\models\Images;
use app\models\PartnersBanners;
use app\models\Post;
use app\models\PostExtras;
use app\models\search\PostSearch;
use app\models\StaticPage;
use app\models\UploadForm;
use app\models\UploadLogoForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [''],
                'rules' => [
                    // [
                    //     'actions' => ['logout'],
                    //     'allow' => true,
                    //     'roles' => ['@'],
                    // ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    public function actionRemove() {
        Post::deleteAll(['autor' => 'guest']);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $countUsers = (string)User::find()->count();
        $countPartners = (string)Post::find()->where(['approve' => 1])->count('id');
        for ($i = 0; $i < 9 - strlen($countUsers); $i++) {
            $countUsers = '0' . $countUsers;
        }
        for ($i = 0; $i < 5 - strlen($countPartners); $i++) {
            $countPartners = '0' . $countPartners;
        }
        $regionCookie = isset($_COOKIE['city']) ? urldecode($_COOKIE['city']) : null;
        $region = null;
        if (isset($regionCookie)) {
            $region = City::findOne(['name' => $regionCookie]);
            if (!isset($region)) {
                $region = Country::findOne(['name' => $regionCookie]);
            }
        }
        // var_dump($region);die();

        return $this->render('index', [
            'vipBanners' => PartnersBanners::getVipBanners($region),
            'premiumBanners' => PartnersBanners::getPremiumBanners($region),
            'classicPosts' => Post::getClassicPosts($region, null),
            'standardPosts' => Post::getStandardPosts($region, null, 100),
            'searchModel' => new PostSearch(),
            'categories' => Category::getAllParentCategories(),
            'countClients' => str_split($countUsers),
            'countPartners' => str_split($countPartners),
            'countCities' => str_split((string)City::find()->count('id')),
            //'countries' => Country::getAllWithCities(),
        ]);
    }

    public function actionCountry($country)
    {
        $country = Country::findOne(['name' => $country]);
        $countryId = $country->id - 1;
        return $this->render('list-city', [
            'vipBanners' => PartnersBanners::find()->where(['group' => '90x1000'])->andWhere(['country' => $countryId])->limit(5)->orderBy('id DESC')->all(),
            'premiumBanners' => PartnersBanners::find()->where(['group' => 'premium'])->andWhere(['country' => $countryId])->limit(15)->orderBy('id DESC')->all(),
            'apartmentPosts' => Post::find()->where(['approve' => 1, 'category' => 313])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$country->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'autoPosts' => Post::find()->where(['approve' => 1, 'category' => 312])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$country->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'eatPosts' => Post::find()->where(['approve' => 1, 'category' => 338])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$country->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'classicPosts' => Post::getClassicPosts($country, null),
            'standardPosts' => Post::getStandardPosts($country, null, 100),
            'categories' => Category::getAllParentCategories(),
            'countries' => Country::getAllWithCities(),
            'searchModel' => new PostSearch(),
            'region' => $country->name
        ]);
    }

    public function actionCity($city)
    {
        $city = City::findOne(['name' => $city]);
        $cityId = $city->id - 1;
        return $this->render('list-city', [
            'vipBanners' => PartnersBanners::find()->where(['group' => '90x1000'])->andWhere(['city' => $cityId])->limit(5)->orderBy('id DESC')->all(),
            'premiumBanners' => PartnersBanners::find()->where(['group' => 'premium'])->andWhere(['city' => $cityId])->limit(15)->orderBy('id DESC')->all(),
//            'standardPosts' => Post::find()->where(['approve' => 1])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$city->name.'%', false])->limit(4)->all(),
            'apartmentPosts' => Post::find()->where(['approve' => 1, 'category' => 313])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$city->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'autoPosts' => Post::find()->where(['approve' => 1, 'category' => 312])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$city->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'eatPosts' => Post::find()->where(['approve' => 1, 'category' => 338])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$city->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'categories' => Category::getAllParentCategories(),
            'classicPosts' => Post::getClassicPosts($city, null),
            'standardPosts' => Post::getStandardPosts($city, null, 100),
            'countries' => Country::getAllWithCities(),
            'searchModel' => new PostSearch(),
            'region' => $city->name
        ]);
    }

    public function actionDistrict ($district)
    {
        $district = District::findOne(['name' => $district]);
        $districtId = $district->id - 1;
        return $this->render('list-city', [
            'vipBanners' => PartnersBanners::find()->where(['group' => '90x1000'])->andWhere(['raion' => $districtId])->limit(5)->orderBy('id DESC')->all(),
            'premiumBanners' => PartnersBanners::find()->where(['group' => 'premium'])->andWhere(['raion' => $districtId])->limit(15)->orderBy('id DESC')->all(),
            'standardPosts' => Post::find()->where(['approve' => 1])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$district->name.'%', false])->limit(4)->all(),
            'apartmentPosts' => Post::find()->where(['approve' => 1, 'category' => 313])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$district->name.'%', false])->limit(4)->all(),
            'autoPosts' => Post::find()->where(['approve' => 1, 'category' => 312])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$district->name.'%', false])->limit(4)->all(),
            'eatPosts' => Post::find()->where(['approve' => 1, 'category' => 338])->andWhere(['like', 'xfields', '%top|top1%', false])->andWhere(['like', 'xfields', '%'.$district->name.'%', false])->orderBy('id DESC')->limit(4)->all(),
            'categories' => Category::getAllParentCategories(),
            'countries' => Country::getAllWithCities(),
            'searchModel' => new PostSearch(),
            'region' => $district->name
        ]);
    }

    public function actionCategory($category)
    {
        $regionCookie = isset($_COOKIE['city']) ? urldecode($_COOKIE['city']) : null;
        $region = null;
        if (isset($regionCookie)) {
            $region = City::findOne(['name' => $regionCookie]);
            if (!isset($region)) {
                $region = Country::findOne(['name' => $regionCookie]);
            }
        }
        /** @var Category $currentCategory */
        $currentCategory = Category::find()->with('single')->where(['alt_name' => $category])->one();
        /** @var Category[] $allCategories */
        $allCategories = Category::find()->alias('cat')->where(['cat.alt_name' => $category])->joinWith('parent p')->select('cat.id')->all();
        return $this->render('list', [
            'vipBanners' => PartnersBanners::getVipBanners($region, $currentCategory),
            'premiumBanners' => PartnersBanners::getPremiumBanners($region, $currentCategory),
            'classicPosts' => Post::getClassicPosts($region, $allCategories),
            'standardPosts' => Post::getStandardPosts($region, $allCategories, 100),
            'guestPosts' => Post::getGuestPosts($region, $currentCategory),
            'currentCategory' => $currentCategory,
            'categories' => Category::getAllParentCategories(),
            'countries' => Country::getAllWithCities(),
            'searchModel' => new PostSearch(),
        ]);
    }

    public function actionPost($post)
    {
        /** @var \app\models\Post $post */
        $post = $this->findPremiumPost($post);
        $relatedPosts = Post::find()->where(['category' => $post->category])->limit(3)->all();
        PostExtras::updateAllCounters(['news_read' => 1], 'news_id=:news_id', [':news_id' => $post->id]);
        return $this->render('post', [
            'model' => $post,
            'relatedPosts' => $relatedPosts,
            'categories' => Category::getAllParentCategories(),
            'countries' => Country::getAllWithCities(),
            'searchModel' => new PostSearch(),
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('search-results', [
            'dataProvider' => $dataProvider,
            'categories' => Category::getAllParentCategories(),
            'countries' => Country::find()->with('cities')->all(),
            'searchModel' => new PostSearch()
        ]);
    }

    public function actionStandard($post)
    {
        /** @var \app\models\Post $post */
        $post = $this->findPost($post);
        PostExtras::updateAllCounters(['news_read' => 1], 'news_id=:news_id', [':news_id' => $post->id]);
        $relatedPosts = Post::find()->where(['category' => $post->category])->limit(3)->all();
        return $this->render('post', [
            'model' => $post,
            'relatedPosts' => $relatedPosts,
            'categories' => Category::getAllParentCategories(),
            'countries' => Country::getAllWithCities(),
            'searchModel' => new PostSearch(),
        ]);
    }

    public function actionUpdateClicked()
    {
        $request = Yii::$app->request;
        $object = $request->post('object');
        $post = $request->post('post');
        PostExtras::updateAllCounters([$object.'_clicked' => 1], 'news_id=:news_id', [':news_id' => $post]);
    }

    public function actionUpdateTable()
    {
//        $sql = "ALTER TABLE for_post ADD COLUMN total_viewed INT(11)";
//        Yii::$app->db->createCommand($sql)->execute();
    }

    public function actionAddCompany()
    {
        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', 'Вы успешно запросили на добавление вашей компаний.');
            $model->refresh();
        }

        return $this->render('add-company', [
            'model' => $model
        ]);
    }

    public function actionGetPostSubcategories($id)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        return ArrayHelper::map(Category::findAll(['parentid' => $id]), 'id', 'name');
    }

    public function actionGetPostDistrict($id)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        return ArrayHelper::merge(['' => ''], ArrayHelper::map(District::findAll(['city_id' => $id]), 'id', 'name'));
    }

    public function actionGetPostCity($id)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        return ArrayHelper::merge(['' => ''], ArrayHelper::map(City::findAll(['country_id' => $id]), 'id', 'name'));
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionPage($name)
    {
        $page = StaticPage::findOne(['name' => $name]);
        $allPages = StaticPage::find()->select('name, descr')->all();
        return $this->render('page', [
            'page' => $page,
            'allPages' => $allPages
        ]);
    }

    public function findPremiumPost($post)
    {
        $post_id = substr($post, 0, strpos($post, '-'));
        $model = Post::findOne($post_id);
        if ($model === null) {
            throw new HttpException(404, 'Post not found');
        }
        return $model;
    }

    public function findPost($post)
    {
        $model = Post::find()->where(['alt_name' => $post])->one();
        if ($model === null) {
            throw new HttpException(404, 'Post not found');
        }
        return $model;
    }
}
